Note: Line with #marie are commands specific to marie's computer.
# Before workshop starts
* write on the whiteboard 
`https://130.238.10.80`
* Open and have ready the file 
`example_webscraping.xls` #marie
* add workshop participant list in the server
# Preparation

1. Plug hdmi screen.
2. If nothing happen click on the screen "import laptop > HDMI"
3. Ctrl+alt+T to open a terminal and do
` sh dell4screen ` #marie
4. Open a browser go to address:
` https://130.238.10.80/user/marie/doc `

# To participants
1. Connect to eduroam
2. Open a browser and launch this address:
` https://130.238.10.80 `
3. Find a folder called "cdhu" click on the appropriate file that will appear below.

# Announces & infos
1. Check out next Uppsala women coding event https://women.uppsala.ai #marie
2. Check up the wiki of CDHU and the Workshop directory
