## What we will learn
1. Where is the command line on your computer?
2. How to navigate folders with commands (cd, ls, pwd ...)
3. How to find the path of your files and folders
4. What is the syntax of path names?
## Links to pedagogical material
All the pedagogical material included these notes are stored in :
[https://gitlab.com/mardub/workshops](https://gitlab.com/mardub/workshops)
(Go to folder "Command_line" and click on "Readme.md")

[https://gitlab.com/mardub/workshops]
Most of the pedagogical material comes from:
[Cultural Analytics tutorial](https://melaniewalsh.github.io/Intro-Cultural-Analytics/01-Command-Line/01-The-Command-Line.html)
and
[Launch school](https://launchschool.com/books/command_line/read/files_directories_executables#clicore)

## Extra things we will learn
- What are the different storage parts of the computer (hard drive, live memory, usb). And also the difference with cloud (for instance, google drive does NOT store on your HD).

- File format: difference between ".txt" vs ".docx" vs "pdf". ".csv" vs ".xlsx"

-



