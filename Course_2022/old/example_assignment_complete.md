[link to assignment](https://docs.google.com/presentation/d/1kUxzC1BU0221svF37h-ap_xnRH0_uWmIybB0kTZ0VIY/edit#slide=id.g13bd8dc8957_0_0)
Part I
1. The lines I need to change are:
```
!python -m spacy download sv_core_news_md
nlp = spacy.load('sv_core_news_md')
```
I would write xx_core_news_md instead
and 'xx_core_news_md'
*Part I
1. a.There is XX unique relevant bigrams with the word "kvinnor"
...

* Part II. 
a. I chose:
Mermaids are birds. Text Mining N.F.S. Grundtvig’s Bestiary
 Katrine Frøkjær Baunvig, Kristoffer Laigaard Nielbo
in  DHNB 2022 Conference : Book of Abstracts. 
http://uu.diva-portal.org/smash/get/diva2:1650296/FULLTEXT01.pdf

This abstract is in the domain of anthropology. In this paper the authors use NLP in order to explore the use a metaphora.

b. The corpus is in Danish.

c. The size they report of their corpus is a bit vague. They mention 1068 publication but we do not know how many words that would represent.

d. They use the following methods:
- word embedding
- tokenization
- lemmatization


The tools they use to implement are:
- the programming language python
...
We do not know what package they used for the tokenization and lemmatisation the reading of the paper itself or maybe an exploration of their personal implementation if open source could give us a hint.
